#!/usr/bin/env python
#
# Convert an entire directory tree of MORAY raw data files to HDF5.
#
"""
%prog [options] topdir

Walk a directory tree starting at TOPDIR and convert any raw MORAY data files
to HDF5. ZIPed directories of MORAY data will be automatically expanded.
"""
from lxml import etree
import os
import re
import numpy
import logging
from optparse import OptionParser
from zipfile import ZipFile
import h5py
import gzip
import glob
from contextlib import contextmanager
from fractions import Fraction

SEQ_NAMESPACE = "http://apl.uw.edu/oe/seq"
SEQ = '{%s}' % SEQ_NAMESPACE
TYPES = {'l': '=i4',
         'f': '=f4'}


class FileNotFound(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


def coroutine(func):
    def wrapper(*args, **kw):
        gen = func(*args, **kw)
        gen.next()
        return gen
    return wrapper


@coroutine
def file_writer(filename, **kwds):
    f = h5py.File(filename, 'w')
    for k, v in kwds.items():
        f.attrs[k] = v
    pings = f.create_group('pings')
    dims = f.create_group('dimensions')
    top = f
    index = 0
    scale = 1.
    channels, scans = 0, 0
    ping = None
    try:
        while True:
            tag, contents = (yield)
            if tag == 'metadata':
                grp = top.create_group('metadata')
                if 'vpeak' in contents:
                    scale = 2. * contents['vpeak']['value'] / (1 << contents['sample_size']['value'])
                for key, obj in contents.items():
                    dset = grp.create_dataset(key, shape=(),
                                              data=obj['value'],
                                              dtype=TYPES[obj['attrs']['type']])
                    for name in obj['attrs']:
                        if name != 'type':
                            dset.attrs[name] = obj['attrs'][name]
            elif tag == 'dimensions':
                channels, scans = contents
                dims.create_dataset('channels', shape=(),
                                    data=channels,
                                    dtype='i4')
                if scans:
                    dims.create_dataset('scans', shape=(),
                                        data=scans,
                                        dtype='i4')
            elif tag == 'attributes':
                for key, val in contents.items():
                    f.attrs[key] = val
            elif tag == 'trigger':
                secs, usecs, uuid, ping_scans = contents
                index += 1
                ping_grp = pings.create_group('ping_%06d' % index)
                top = ping_grp
                if not ping_scans:
                    ping_scans = scans
                else:
                    ping_grp.create_group('dimensions'
                                          ).create_dataset('scans',
                                                           shape=(),
                                                           data=ping_scans,
                                                           dtype='i4')
                ping = ping_grp.create_dataset('signal',
                                               shape=(ping_scans, channels),
                                               dtype='i4')
                ping_grp.attrs['timestamp'] = long(secs) * 1000000L + long(usecs)
                ping_grp.attrs['uuid'] = uuid
                ping.attrs['scale'] = scale
                ping.attrs['units'] = 'counts'
                logging.info('New trigger: %s (%d)', uuid, index)
            elif tag == 'data':
                row, scan = contents
                ping[row, :] = numpy.array([int(s) for s in scan.split()],
                                           numpy.int32)
            elif tag == 'rawdata':
                start, end, rawscan = contents
                ping[start:end, :] = rawscan
            elif tag == 'nodata':
                del ping_grp['signal']
            elif tag == 'close':
                logging.info('Trigger processed')
    finally:
        dims.create_dataset('npings', shape=(),
                            data=index,
                            dtype='i4')
        f.close()


def sign_extend(x):
    """
    Sign extend a vector to 24-bit values stored in 32-bit words
    """
    sign_bit = (1 << 23)
    mask = (1 << 24) - 1
    # Select the negative values
    neg = (x & sign_bit) != 0
    # Select the positive values
    pos = -neg

    # Sign-extend the negative values
    tmp = x[neg] | ~mask
    # The 'astype' cast is required on 64-bit systems
    x[neg] = tmp.astype(numpy.int32)
    # Keep the lower 24-bits of the positive values
    tmp = x[pos] & mask
    x[pos] = tmp
    return x


def find_binary_file(pathname):
    """
    Try to locate the binary file. It can be at the full pathname or
    in the local directory. It might also have been gzip compressed,
    in which case the filename will have a '.gz' appended.
    """
    def try_open(fname):
        try:
            if fname.endswith('.gz'):
                return gzip.GzipFile(fname, 'rb')
            else:
                return open(fname, 'rb')
        except IOError:
            return None
    basename = os.path.basename(pathname)
    names = [pathname,
             pathname + '.gz',
             basename,
             basename + '.gz']
    for name in names:
        f = try_open(name)
        if f is not None:
            logging.info('Opening binary file %s', name)
            return f
    raise FileNotFound('Cannot find binary data file, %s' % basename)


def parse_binary_file(infile, master_data, slave_data, block_size):
    """
    Generator to parse a raw binary data file and return the next block of
    scans on each iteration. Each scan is a numpy array of 32-bit integer
    values (A/D counts)
    """
    m_chans, m_mask = master_data
    s_chans, s_mask = slave_data
    ratio = Fraction(m_chans, m_chans + s_chans)
    m_index = [((m_mask & (1 << i)) != 0) for i in range(m_chans)]
    s_index = [((s_mask & (1 << i)) != 0) for i in range(s_chans)]
    # Number of bytes per block
    n_bytes = (m_chans + s_chans) * block_size * 4
    block = infile.read(n_bytes)
    while block:
        # Separate the block into data from the master board and
        # data from the slave board
        n = int(len(block) * ratio)
        m_data = numpy.fromstring(block[0:n], dtype=numpy.int32)
        s_data = numpy.fromstring(block[n:], dtype=numpy.int32)
        # Number of scans in this block
        scans_per_block = len(m_data) / m_chans
        # Sign extend the values, reformat as a 2-d matrix where each row is a
        # separate scan.
        m_data = sign_extend(m_data).reshape((scans_per_block, m_chans))
        s_data = sign_extend(s_data).reshape((scans_per_block, s_chans))
        # Remove the unused channels
        m_data = numpy.compress(m_index, m_data, axis=1)
        # Do the same to the slave data and join with the master
        m_data = numpy.hstack((m_data, numpy.compress(s_index, s_data, axis=1)))
        yield m_data
        block = infile.read(n_bytes)


def strip_ns(tag):
    """
    Remove XML namespace info from a tag
    """
    ns, tag = tag.split('}')
    return tag


def parse_section(elem):
    result = {}
    for child in elem:
        try:
            value = eval(child.text)
        except NameError:
            value = child.text
        d = {'attrs': dict(child.attrib),
             'value': value}
        result[strip_ns(child.tag)] = d
    return result


def parse_attributes(elem):
    d = {}
    # All child elements must have an empty namespace
    for child in elem:
        try:
            d[child.tag] = eval(child.text)
        except NameError:
            d[child.tag] = child.text
    return d


def parse_file(infile, writer):
    """
    Iterative parser for moray_recv XML data files. Sends messages to the
    writer coroutine which will construct the output file.
    """
    n_chans = 0
    n_scans = 0
    bs = 0
    index = 0
    metadata = None
    master_data = None
    slave_data = None
    scan_source = None
    attributes = {}
    for event, element in etree.iterparse(infile,
                                          events=('start', 'end')):
        if (event, element.tag) == ('start', SEQ + 'sequence'):
            writer.send(('sequence', element.get('timestamp')))
        elif (event, element.tag) == ('end', SEQ + 'metadata'):
            metadata = parse_section(element)
            writer.send(('metadata', metadata))
        elif (event, element.tag) == ('end', SEQ + 'attributes'):
            attributes = parse_attributes(element)
            writer.send(('attributes', attributes))
        elif (event, element.tag) == ('end', SEQ + 'channels'):
            n_chans = int(element.text)
            master_data = [int(e) for e in element.get('master').split(',')]
            slave_data = [int(e) for e in element.get('slave').split(',')]
        elif (event, element.tag) == ('end', SEQ + 'scans'):
            n_scans = int(element.text)
        elif (event, element.tag) == ('end', SEQ + 'block_size'):
            bs = int(element.text)
        elif (event, element.tag) == ('end', SEQ + 'dimensions'):
            writer.send(('dimensions', (n_chans, n_scans)))
        elif (event, element.tag) == ('end', SEQ + 'scan'):
            writer.send(('data', (index, element.text.strip())))
            index = index + 1
            element.clear()
        elif (event, element.tag) == ('start', SEQ + 'trigger'):
            secs, usecs = element.get('timestamp').split('.')
            if element.get('scans'):
                n_scans = int(element.get('scans'))
            writer.send(('trigger', (secs, usecs, element.get('id'),
                                     n_scans)))
        elif (event, element.tag) == ('end', SEQ + 'trigger'):
            assert n_chans > 0 and n_scans > 0
            index = 0
            if scan_source is not None:
                for scan in scan_source:
                    rows, cols = scan.shape
                    writer.send(('rawdata', (index, index + rows, scan)))
                    index = index + rows
                    if index >= n_scans:
                        break
            writer.send(('close', None))
            element.clear()
        elif (event, element.tag) == ('end', SEQ + 'datafile'):
            assert master_data is not None
            assert slave_data is not None
            assert bs > 0
            try:
                infile = find_binary_file(element.text)
                scan_source = parse_binary_file(infile, master_data, slave_data, bs)
            except FileNotFound:
                logging.info('Skipping missing file: %s', element.text)
                scan_source = None
                writer.send(('nodata', None))

    writer.close()


@contextmanager
def pushd(dirname):
    """
    Context manager to temporarily work in a new directory.
    """
    cwd = os.getcwd()
    os.chdir(dirname)
    try:
        yield
    finally:
        os.chdir(cwd)


def expand_archive(archive_name):
    """
    Expand a ZIP archive after verifying that none of the members use absolute paths.
    Return the toplevel directory of the archive.
    """
    z = ZipFile(archive_name, 'r')
    namelist = [e for e in z.namelist() if not (e.startswith('/') or '..' in e)]
    basedir = (namelist[0].split('/'))[0]
    # Work-around a bug in ZipFile.extractall on some platforms
    for name in namelist:
        if name.endswith('/'):
            try:
                os.makedirs(name)
            except OSError:
                pass
        else:
            z.extract(name)
    z.close()
    return basedir


def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(keep=True)
    parser.add_option('-c', '--clean',
                      action='store_false',
                      dest='keep',
                      help='remove raw files after converting')
    opts, args = parser.parse_args()
    if len(args) < 1:
        parser.error('Missing arguments')

    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        datefmt='%Y/%m/%d %H:%M:%S')

    topdir = args[0]
    for root, dirs, files in os.walk(topdir):
        for f in files:
            if re.match(r'\d{8}_\d{6}(_subset)*\.zip', f, re.IGNORECASE):
                # Expand all ZIP archives and add to directory list
                arfile = os.path.join(root, f)
                logging.info('Expanding ZIP archive %s', arfile)
                with pushd(root):
                    newdir = expand_archive(f)
                if os.path.isdir(os.path.join(root, newdir)):
                    dirs.append(newdir)
                    logging.info('Removing %s', arfile)
                    os.unlink(arfile)
            elif re.match(r'data_\d{8}_\d{6}_\d{3}\.xml', f, re.IGNORECASE):
                # Process MORAY XML data files
                xmlfile = os.path.join(root, f)
                logging.info('Processing %s', xmlfile)
                basename, ext = os.path.splitext(f)
                binfile_pattern = basename + '*.bin*'
                outfile = basename + '.h5'
                with pushd(root):
                    writer = file_writer(outfile)
                    parse_file(open(f, 'r'), writer)
                    if not opts.keep:
                        logging.info('Removing xml file: %s', f)
                        os.unlink(f)
                        for binfile in glob.glob(binfile_pattern):
                            logging.info('Removing binary file: %s', binfile)
                            os.unlink(binfile)


if __name__ == '__main__':
    main()
