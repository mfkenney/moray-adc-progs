/*
** MORAY A/D data-acquistion program
*/
class Trigger {
	unowned Moray.Adc	adc;
	uint delay_usec;
	int interval_usec;
	
	public Trigger(Moray.Adc adc, double interval, uint delay=2) {
		double t = interval*1000000;
		
		this.adc = adc;
		this.delay_usec = delay*1000000;
		this.interval_usec = (int)t;
	}

	public void* run() {
		uint t = this.delay_usec;
		Thread.usleep(t);
		t = this.interval_usec;
		
		while(true)
		{
			this.adc.trigger();
			Thread.usleep(t);
		}
		
	}
}

// Provide a list iterator which when it reaches the end, continues to
// return the last element.
public class InfiniteIterator {
	Gee.Iterator<string>	list;
	string					last_value = null;
	
	public InfiniteIterator(string? slist) {
		var container = new Gee.ArrayList<string> ();

		if(slist != null) {
			string[] tokens = slist.split(",");
			for(int i = 0;i < tokens.length;i++) {
				container.add(tokens[i]);
			}
		}
		
		this.list = container.iterator();
	}

	public unowned string? get_next() {
		if(this.list.next() == true) 
			this.last_value = this.list.get();
			
		return this.last_value;
	}
	
}

// Generate a sequence of filenames with an increasing index
// number. The supplied filename template must a printf-style
// format string with a '%d' somewhere inside it. This pattern
// will be replaced by the index number.
public class FilenameSequence {
	string	template;
	int		index;
	bool	constant;
	string	rval;
	
	public FilenameSequence(string template, int start = 1) {
		this.template = template;
		this.index = start;
		if(!("%d" in template))
			constant = true;
		else
			constant = false;
	}

	public unowned string get_next() {
		if(this.constant)
			return this.template;
		else 
		{
			this.rval = this.template.printf(this.index);
			this.index++;
			return this.rval;
		}
		
	}
	
}


public class MorayReceiver {
	static bool verbose;
	static bool do_pause;
	static bool terse = false;
	static uint max_level = LogLevelFlags.LEVEL_INFO;
	static FileStream out_stream;
	static Stdio.BinaryStream data_stream = null;
	static string data_file = null;
	static string current_file = null;
	static uint trig_timeout = 60;
	static double v_range = 5;
	static int gain = -1;
	static uint n_master_channels = 8;
	static uint n_slave_channels = 4;
	static uint n_scans_out = 0;
	static uint trig_count = 0;
	static uint scans_per_block = 10;
	static uint pre_acq_delay = 0;
	static double delay = 0;
	static double trig_interval = 0;
	static string gain_values = null;
	static string delay_values = null;
	
	const OptionEntry[] options = {
		{ "verbose", 'v', 0, OptionArg.NONE, ref verbose, "Print more diagnostic info to stderr", null},
		{ "pause", 'p', 0, OptionArg.NONE, ref do_pause, "Pause before arming A/D boards", null},
		{ "master", 'm', 0, OptionArg.INT, out n_master_channels, 
		  "Number of channels to read on Master (board 0)", 
		  null},
		{ "slave", 's', 0, OptionArg.INT, out n_slave_channels, 
		  "Number of channels to read on Slave (board 1)", 
		  null},
		{ "count", 'c', 0, OptionArg.INT, out trig_count, "Number of triggers to wait for", null},
		{ "timeout", 't', 0, OptionArg.INT, out trig_timeout, 
		  "Trigger timeout in seconds (default 60)", null},
		{ "gain", 'g', 0, OptionArg.STRING, ref gain_values, "Input gain setting in dB", null},
		{ "interval", 'i', 0, OptionArg.DOUBLE, out trig_interval, "Software trigger interval in seconds", null},
		{ "range", 'r', 0, OptionArg.DOUBLE, out v_range, "Input voltage range: 2.5, 5, or 10", null},
		{ "delay", 'd', 0, OptionArg.STRING, ref delay_values, "Pre-acquisition delay in seconds", null},
		{ "binary", 'b', 0, OptionArg.FILENAME, ref data_file, "Specify a binary output file", null},
		{ "block_size", 0, 0, OptionArg.INT, out scans_per_block, 
		  "Number of scans per data block (buffer)", null},
		{ "terse", 0, 0, OptionArg.NONE, ref terse, "Use terse prompts", null},
		{ null }
        };

	
	static char prompt_user(ref long value) {
		if(terse)
			stdout.puts("?");
		else
			stdout.puts("[n:next c:continue q:quit]=>");
		stdout.flush();
		string response = stdin.read_line();
		if(response != null) 
		{
			if(Posix.isdigit(response[0]))
			{
				value = long.parse(response);
				return 'c';
			}
			
			return response[0];
		}
		
		return 0;
	}
	
	static string get_timestamp() {
		Posix.timeval tv = Posix.timeval();
		tv.get_time_of_day();
		return "%ld.%06ld".printf(tv.tv_sec, tv.tv_usec);
	}
	
	static void start_sequence(uint fsamp, uint data_size, double vpeak, 
							   uint channels, Moray.Adc adc) {
		string	ts = get_timestamp();
		
		out_stream.puts(@"<seq:sequence timestamp=\"$ts\" xmlns:seq=\"http://apl.uw.edu/oe/seq\">\n");
		out_stream.puts("<seq:metadata>\n");
		out_stream.puts(@"<seq:fsample type=\"l\" units=\"hz\">$fsamp</seq:fsample>\n");
		out_stream.puts(@"<seq:vpeak type=\"f\" units=\"volts\">$vpeak</seq:vpeak>\n");
		out_stream.puts(@"<seq:sample_size type=\"l\" units=\"bits\">$data_size</seq:sample_size>\n");
		out_stream.puts("</seq:metadata>\n");
		out_stream.puts("<seq:dimensions>\n");
		out_stream.printf("<seq:channels master=\"%u,%u\" slave=\"%u,%u\">%u</seq:channels>\n",
						  adc.master_channels_read, adc.master_mask,
						  adc.slave_channels_read, adc.slave_mask,
						  channels);
		out_stream.printf("<seq:block_size>%u</seq:block_size>\n", adc.scans_per_block);
		out_stream.puts("</seq:dimensions>\n");
	}

	static void trigger_metadata(int gain_val, double delay_val) {
		out_stream.puts("<seq:metadata>\n");
		out_stream.puts(@"<seq:gain type=\"l\" units=\"dB\">$gain_val</seq:gain>\n");
		out_stream.printf("<seq:delay type=\"f\" units=\"seconds\">%.6f</seq:delay>\n", delay_val);
		out_stream.puts("</seq:metadata>\n");
	}
	
	static void end_sequence() {
		out_stream.puts("</seq:sequence>\n");
	}

	static void record_data_file(string filename) {
		out_stream.puts(@"<seq:datafile>$filename</seq:datafile>\n");
	}
	
	static void trigger_handler(int state, char[] token) {
		if(state == Moray.TRIG_START) 
		{
			string id = (string)token;
			string	ts = get_timestamp();
			out_stream.puts(@"<seq:trigger scans=\"$n_scans_out\" timestamp=\"$ts\" id=\"$id\">\n");
			trigger_metadata(gain, delay);
			if(current_file != null)
				record_data_file(current_file);
		}
		else if(state == Moray.TRIG_END)
			out_stream.puts("</seq:trigger>\n");
	}

    static void write_scan(Moray.Scan *sp, uint master_chans, uint master_mask, 
						   uint slave_chans, uint slave_mask, int n_scans) {
		int		i, j;
		uint	mask;
		uint32	*m_buf;
		uint32	*s_buf;

		m_buf = sp->master;
		s_buf = sp->slave;
		
		for(j = 0;j < n_scans;j++) 
		{
			if(pre_acq_delay > 0)
			{
				pre_acq_delay--;
				continue;
			}

			out_stream.puts("<seq:scan>");
		
			if(master_mask != 0) 
			{
				for(i = 0,mask = 1;i < master_chans;i++,m_buf++,mask <<= 1) {
					if((mask & master_mask) == mask)
						out_stream.printf("%d ", Moray.DATA_VALUE(*m_buf));
				}
			}

			if(slave_mask != 0) 
			{
				for(i = 0,mask = 1;i < slave_chans;i++,s_buf++,mask <<= 1) {
					if((mask & slave_mask) == mask)
						out_stream.printf("%d ", Moray.DATA_VALUE(*s_buf));
				}
			}

			out_stream.puts("</seq:scan>\n");
		}
		
	}

    static void write_binary_scan(Moray.Scan *sp, uint master_chans, uint master_mask, 
								  uint slave_chans, uint slave_mask, int n_scans) {
		uint32	*m_buf;
		uint32	*s_buf;

		m_buf = sp->master;
		s_buf = sp->slave;

		if(pre_acq_delay > 0)
		{
			if(pre_acq_delay >= n_scans)
			{
				// Skip this entire block
				pre_acq_delay -= n_scans;
				return;
			}
			pre_acq_delay = 0;
		}
		
		
		if(master_mask != 0)
			data_stream.bwrite((void*)m_buf, 4, master_chans*n_scans);

		if(slave_mask != 0)
			data_stream.bwrite((void*)s_buf, 4, slave_chans*n_scans);

	}

	static void add_attributes(string[] args) {
		string[]	entry;
		
		out_stream.puts("<seq:attributes>\n");
		foreach(string arg in args) 
		{
			entry = arg.split("=", 2);
			out_stream.printf("<%s>%s</%s>\n", entry[0], entry[1], entry[0]);
		}
		out_stream.puts("</seq:attributes>\n");
	}
	
	public static int main(string[] args) {
		string usage = """
On each trigger, read WINDOW seconds of data from the A/D boards at sample
rate RATE hz. The data are streamed to OUTFILE an XML-encoded format.

The optional parameters ATTR=VALUE are used to add metadata to the output file.
""";

		if (!Thread.supported()) 
		{
			stderr.printf("Cannot run without threads.\n");
			return 1;
		}

		try	{
			var context = new OptionContext("RATE WINDOW OUTFILE [ATTR=VALUE ...]");
			context.set_summary(usage);
			context.add_main_entries(options, null);
			context.set_help_enabled(true);
			context.parse(ref args);
		} catch(OptionError e) 	{
			stderr.printf("%s\n", e.message);
			stderr.printf("Run '%s --help' to see usage message\n", args[0]);
			return 1;
	    }
	
		if(args.length < 4) 
		{
			stderr.puts(usage);
			return 1;
		}

		if(verbose)
			max_level = LogLevelFlags.LEVEL_DEBUG;

		Log.set_default_handler(
			(domain, flags, msg) => {
				uint level = flags & LogLevelFlags.LEVEL_MASK;
				if(level > max_level)
					return;
				Time now = Time();
				now = Time.local(time_t());
				stderr.printf("[%s] %s\n", now.to_string(), msg);
			}
			);

		dynamic DBus.Object gain_ctl;
		try {
			DBus.Connection conn = DBus.Bus.get(DBus.BusType.SYSTEM);
			gain_ctl = conn.get_object("org.moray.Gain", "/", "org.moray.Gain");
			gain = gain_ctl.get_gain();
			
		} catch(DBus.Error e) {
			error(e.message);
		}
		
		out_stream = FileStream.open(args[3], "w");


		uint rate = int.parse(args[1]);
		var window_list = new InfiniteIterator(args[2]);
		var gain_list = new InfiniteIterator(gain_values);
		var delay_list = new InfiniteIterator(delay_values);
		
		
		// Sanity check the input voltage range
		if(v_range > 5)
			v_range = 10;
		else if(v_range > 2.5)
			v_range = 5;
		else
			v_range = 2.5;

		var adc = new Moray.Adc(Moray.devname(0), (1 << n_master_channels)-1, 
								(n_slave_channels > 0) ? Moray.devname(1) : null, 
								(1 << n_slave_channels)-1,
								scans_per_block);
		
		rate = (uint)adc.init(rate, 48, 10, (float)v_range);

		start_sequence(rate, Moray.DATA_SIZE, v_range,
                       n_master_channels+n_slave_channels, adc);
		
		if(args.length > 3)
			add_attributes(args[4:args.length]);

		Moray.ScanFunc writer = write_scan;
		FilenameSequence fileseq = null;
		
		if(data_file != null) 
		{
			writer = write_binary_scan;
			// The specified filename can be a template in which case
			// we will be writing a new file for each trigger.
			if("%d" in data_file)
				fileseq = new FilenameSequence(data_file);
			else 
			{
				message("Opening binary output file %s", data_file);
				data_stream = Stdio.BinaryStream.open(data_file, "w");
				record_data_file(data_file);
			}
		}		
		

		long n_read = 0;
		long next = 1;
		long limit = trig_count;
		char cmd_code;
		bool use_hardware = true;
		
		if(trig_interval > 0) 
		{
			// Software trigger initialization
			var trig = new Trigger(adc, trig_interval);
		
			// Start thread to trigger the ADC
			try {
				Thread.create<void*>(trig.run, false);
			} catch(ThreadError e) {
				error("%s\n", e.message);
			}
			message("Using a software trigger interval of %.3f seconds", trig_interval);
			use_hardware = false;
		}

		uint skip_scans;
		int	rval = 0;
		uint n_scans_in = 0;
		double window;

		// Use a real-time scheduling policy with a relatively high priorityto insure we can
		// pull data off the A/D boards before the buffers overflow. In order to do this, the
		// process must be running with CAP_SYS_NICE capabilities.
		Posix.Sched.Param s_params = {80};
		if(Posix.Sched.setscheduler(0, Posix.Sched.Algorithm.FIFO, ref s_params) < 0)
			message("Warning: cannot set realtime priority");
		
								 
		// Trigger Loop
		do {
			// Get and parse the next value for each of the
			// per-trigger settings.
			string s = null;
			s = window_list.get_next();
			if(s == null)
				error("No window length specified");
			
			window = double.parse(s);
			s = delay_list.get_next();
			if(s != null)
				delay = double.parse(s);
			s = gain_list.get_next();
			if(s != null) 
			{
				gain = int.parse(s);
				gain_ctl.set_gain(gain);
			}

			if(fileseq != null)
			{
				// Open the next binary data file
				current_file = fileseq.get_next();
				message("Opening binary output file %s", current_file);
				if(data_stream != null)
					data_stream.bflush();
				data_stream = Stdio.BinaryStream.open(current_file, "w");
			}
			
			skip_scans = (int)(delay * rate);
			if(skip_scans > 0 && data_file != null)
			{
				// Since we are writing to a binary file, we need to
				// round the pre-acquisition delay down to a block-size
				// boundary
				skip_scans = (uint)(skip_scans/scans_per_block) * scans_per_block;
				delay = skip_scans/(double)rate;
			}
			
			// Pre_acq_delay will be decremented by the output function
			pre_acq_delay = skip_scans;
			n_scans_out = (uint)(window * rate);

			// Add pre-acquisition delay to the window size
			n_scans_in = n_scans_out + skip_scans;

			cmd_code = 'c';
			if(do_pause)
				cmd_code = prompt_user(ref limit);
			switch(cmd_code) 
			{
				case 'q':
					n_read = 0;
					break;
				case 'c':
					do_pause = false;
					adc.arm(trig_timeout*1000, use_hardware);
					stderr.printf("Ping: %3ld - Waiting for trigger ...\n", next);
					n_read = adc.read(n_scans_in, writer, trigger_handler);
					next++;
					break;
				case 'n':
 					adc.arm(trig_timeout*1000, use_hardware);
					stderr.printf("Ping: %3ld - Waiting for trigger ...\n", next);
					n_read = adc.read(n_scans_in, writer, trigger_handler);
					next++;
					break;
				default:
					break;
			}

			if(n_read != n_scans_in) 
				rval = 1;
			
			// Limit reached, force an exit from the loop
			if(limit > 0 && next > limit)
				n_read = 0;

		} while(n_read == n_scans_in);
		
		if(data_stream != null)
			data_stream.bflush();
		end_sequence();
		
		return rval;
	}

}
