/*
 * Use one of the EMM8 DIO lines to simulate a SYNC (trigger)
 */
#include <sys/io.h>
#include <unistd.h>

#define BASE_ADDR	0x140
#define NPORTS		8
#define DIR_REG		2
#define IO_REG		3
#define IO_A		0x01

int main(int ac, char **av)
{
    unsigned	delay = 100;

    if(ac > 1)
	delay = atoi(av[1]);
    
    ioperm(BASE_ADDR, NPORTS, 1);
    
    outb(0, BASE_ADDR+IO_REG);
    outb(IO_A, BASE_ADDR+DIR_REG);
    usleep(1000);

    outb(IO_A, BASE_ADDR+IO_REG);
    usleep(delay);
    outb(0, BASE_ADDR+IO_REG);

    return 0;
}
