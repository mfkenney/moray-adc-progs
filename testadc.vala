/*
** Simple ADC read test.
*/

class Trigger {
	unowned Moray.Adc	adc;
	uint delay_usec;
	int interval_usec;
	uint count;
	
	public Trigger(Moray.Adc adc, double interval, uint count, uint delay=2) {
		double t = interval*1000000;
		
		this.adc = adc;
		this.delay_usec = delay*1000000;
		this.interval_usec = (int)t;
		this.count = count;
	}

	public void* run() {
		uint t = this.delay_usec;
		for(int i = 0;i < this.count;i++) 
		{
			Thread.usleep(t);
			this.adc.trigger();
			if(i == 0)
				t = this.interval_usec;
		}
		
		return null;
	}
}

public class Testadc {
	static bool verbose;
	static uint max_level = LogLevelFlags.LEVEL_INFO;
	static unowned FileStream out_stream;
	static uint n_master_channels = 8;
	static uint n_slave_channels = 4;
	static uint trig_count = 1;
	static double trig_interval = 2;
	static double v_range = 5;
	static bool new_trigger;
	
	const OptionEntry[] options = {
		{ "verbose", 'v', 0, OptionArg.NONE, ref verbose, "Print more diagnostic info to stderr", null},
		{ "master", 'm', 0, OptionArg.INT, out n_master_channels, 
		  "Number of channels to read on Master (board 0)", 
		  null},
		{ "slave", 's', 0, OptionArg.INT, out n_slave_channels, 
		  "Number of channels to read on Slave (board 1)", 
		  null},
		{ "count", 'c', 0, OptionArg.INT, out trig_count, "Number of triggers", null},
		{ "interval", 'i', 0, OptionArg.DOUBLE, out trig_interval, "Trigger interval in seconds", null},
		{ "range", 'r', 0, OptionArg.DOUBLE, out v_range, "Input voltage range: 2.5, 5, or 10", null},
		{ null }
        };

	static string get_timestamp() {
		Posix.timeval tv = Posix.timeval();
		tv.get_time_of_day();
		return "%ld.%06ld".printf(tv.tv_sec, tv.tv_usec);
	}
	
	static void start_sequence(uint fsamp, uint data_size, double vpeak, 
							   uint scans, uint channels) {
		string	ts = get_timestamp();
		
		out_stream.puts(@"<seq:sequence timestamp=\"$ts\" xmlns:seq=\"http://apl.uw.edu/oe/seq\">\n");
		out_stream.puts("<seq:metadata>\n");
		out_stream.puts(@"<seq:fsample type=\"l\" units=\"hz\">$fsamp</seq:fsample>\n");
		out_stream.puts(@"<seq:vpeak type=\"f\" units=\"volts\">$vpeak</seq:vpeak>\n");
		out_stream.puts(@"<seq:sample_size type=\"l\" units=\"bits\">$data_size</seq:sample_size>\n");
		out_stream.puts("</seq:metadata>\n");
		out_stream.puts("<seq:dimensions>\n");
		out_stream.puts(@"<seq:scans>$scans</seq:scans>\n");
		out_stream.puts(@"<seq:channels>$channels</seq:channels>\n");
		out_stream.puts("</seq:dimensions>\n");
	}
	
	static void end_sequence() {
		out_stream.puts("</seq:sequence>\n");
	}
	
	static void start_trigger() {
		string	ts = get_timestamp();
		out_stream.puts(@"<seq:trigger timestamp=\"$ts\">\n");
	}

	static void end_trigger() {
		out_stream.puts("</seq:trigger>\n");
	}
	
    static void write_scan(Moray.Scan *sp, uint master_mask, uint slave_mask) {
		int		i;
		uint	mask;
		unowned Array<uint32> a;

		if(new_trigger)
		{
			start_trigger();
			new_trigger = false;
		}
		
		out_stream.puts("<seq:scan>");
		
		if(master_mask != 0) {
			a = sp->master;
			for(i = 0,mask = 1;i < a.length;i++,mask <<= 1) {
				if((mask & master_mask) == mask)
					out_stream.printf("%d ", Moray.DATA_VALUE(a.index(i)));
			}
		}

		if(slave_mask != 0) {
			a = sp->slave;
			for(i = 0,mask = 1;i < a.length;i++,mask <<= 1) {
				if((mask & slave_mask) == mask)
					out_stream.printf("%d ", Moray.DATA_VALUE(a.index(i)));
			}
		}

		out_stream.puts("</seq:scan>\n");
	}

	static void add_attributes(string[] args) {
		string[]	entry;
		
		out_stream.puts("<seq:attributes>\n");
		foreach(string arg in args) 
		{
			entry = arg.split("=", 2);
			out_stream.printf("<%s>%s</%s>\n", entry[0], entry[1], entry[0]);
		}
		out_stream.puts("</seq:attributes>\n");
	}
	
	public static int main(string[] args) {
		string usage = """
On each software trigger, read WINDOW seconds of data from the 12 MORAY A/D
channels (8 from the master, 4 from the slave) at sample rate RATE hz. The
data are streamed to standard output in an XML-encoded format. The optional
ATTR=VALUE arguments can be used to add arbitrary metadata to the output.

Command-line options can be used to set the number of triggers and the interval.
""";

		if (!Thread.supported()) {
			stderr.printf("Cannot run without threads.\n");
			return 1;
		}

		try	{
			var context = new OptionContext("RATE WINDOW [ATTR=VALUE ...]");
			context.set_summary(usage);
			context.add_main_entries(options, null);
			context.set_help_enabled(true);
			context.parse(ref args);
		} catch(OptionError e) 	{
			stderr.printf("%s\n", e.message);
			stderr.printf("Run '%s --help' to see usage message\n", args[0]);
			return 1;
	    }
	
		if(args.length < 3) {
			stderr.puts(usage);
			return 1;
		}

		if(verbose)
			max_level = LogLevelFlags.LEVEL_DEBUG;

		Log.set_default_handler(
			(domain, flags, msg) => {
				uint level = flags & LogLevelFlags.LEVEL_MASK;
				if(level > max_level)
					return;
				Time now = Time();
				now = Time.local(time_t());
				stderr.printf("[%s] %s\n", now.to_string(), msg);
			}
			);
		
		out_stream = stdout;

		uint rate = args[1].to_int();
		double window = args[2].to_double();
		uint nscans;

		// Sanity check the input voltage range
		if(v_range > 5)
			v_range = 10;
		else if(v_range > 2.5)
			v_range = 5;
		else
			v_range = 2.5;

		// Read the first 8 channels of the master and the first 4 of the slave.
		var adc = new Moray.Adc(Moray.devname(0), (1 << n_master_channels)-1, 
								Moray.devname(1), (1 << n_slave_channels)-1);
		rate = (uint)adc.init(rate, 1024, 10, (float)v_range);
		nscans = (uint)(window * rate);
	
		start_sequence(rate, Moray.DATA_SIZE, v_range, nscans, n_master_channels+n_slave_channels);
		if(args.length > 3)
			add_attributes(args[3:args.length]);
		
		var trig = new Trigger(adc, trig_interval, trig_count);
		
		// Start thread to trigger the ADC
		try {
			Thread.create(trig.run, false);
		} catch(ThreadError e) {
			error("%s\n", e.message);
		}

		long n_read = 0;
		
		while(trig_count > 0) {
			adc.arm(10*1000);
			new_trigger = true;
			stderr.puts("Reading samples ...\n");
			n_read = adc.read(nscans, write_scan);
			if(n_read > 0)
				end_trigger();
			trig_count--;
		}
		
		end_sequence();
		
		return 0;
	}

}
