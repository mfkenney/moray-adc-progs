#!/usr/bin/env python
#
# Convert the raw SAMS XML data file to a directory of text or netcdf files.
#
"""
%prog [options] samsfile

Export the contents of a SAMS XML data file to a directory of netcdf or
ascii text data files. Each trigger is stored in a separate file.
"""
import sys
from lxml import etree
import json
import os
import csv
import time
import numpy
import logging
from optparse import OptionParser
from Scientific.IO.NetCDF import NetCDFFile

SEQ_NAMESPACE = "http://apl.uw.edu/oe/seq"
SEQ = '{%s}' % SEQ_NAMESPACE

def coroutine(func):
    def wrapper(*args, **kw):
        gen = func(*args, **kw)
        gen.next()
        return gen
    return wrapper

@coroutine
def nc_file_writer(basedir, file_template='SAM_SIG_I{0:05d}.nc'):
    """
    Create a directory of netCDF files, one file for each trigger. Sequence
    metadata is stored in each file.
    """
    findex = 1
    try:
        while True:
            tag, contents = (yield)
            if tag == 'sequence':
                secs, usecs = [int(s) for s in contents.split('.')]
                ts = time.strftime('SAMS_SIG_%m-%d-%y_%H-%M-%S', time.localtime(secs))
                dirname = os.path.join(basedir, '{0}-{1:03d}'.format(ts, int(usecs/1000)))
                logging.info('New directory %s', dirname)
                try:
                    os.makedirs(dirname)
                except:
                    pass
            elif tag == 'metadata':
                metadata, timestamp, channels, scans = contents
                secs, usecs = [int(s) for s in timestamp.split('.')]
                filename = os.path.join(dirname, file_template.format(findex))
                logging.info('New datafile %s', filename)
                nc = NetCDFFile(filename, 'w')
                nc.createDimension('channel', channels)
                nc.createDimension('scan', None)
                v = nc.createVariable('time', 'l', ())
                setattr(v, 'long_name', 'acquisition start time')
                setattr(v, 'units', 'seconds since 1970-01-01 00:00:00 -0')
                v.assignValue(secs)
                v = nc.createVariable('usecs', 'l', ())
                setattr(v, 'long_name', 'acquisition start time fraction')
                setattr(v, 'units', 'microseconds')
                v.assignValue(usecs)
                for name, entry in metadata.items():
                    if name == '__attrs__':
                        for key, val in entry.items():
                            setattr(nc, key, val)
                    else:
                        v = nc.createVariable(name, entry['attrs']['type'], ())
                        v.assignValue(entry['value'])
                        for key, val in entry['attrs'].items():
                            if key != 'type':
                                setattr(v, key, val)
                v = nc.createVariable('rdata', 'l', ('scan', 'channel'))
                setattr(v, 'units', 'volts')
                setattr(v, 'long_name', 'A/D samples')
                scale = 2.*metadata['vpeak']['value']/(1 << metadata['sample_size']['value'])
                setattr(v, 'scale_factor', scale)
            elif tag == 'data':
                idx, scan = contents
                v[idx, :] = numpy.array([int(s) for s in scan.split()], numpy.int32)
            elif tag == 'rawdata':
                idx, rawscan = contents
                v[idx, :] = rawscan[:]
            elif tag == 'close':
                nc.close()
                findex += 1
    except GeneratorExit:
        pass


@coroutine
def text_file_writer(basedir, file_template='SAM_SIG_I{0:05d}.txt'):
    """
    Create a directory of ASCII text data files, one file per trigger. Sequence metadata
    is stored in JSON format in a file named 'metadata.json'
    """
    findex = 1
    metadata = {}
    ofile = None
    try:
        while True:
            tag, contents = (yield)
            if tag == 'sequence':
                secs, usecs = [int(s) for s in contents.split('.')]
                ts = time.strftime('SAMS_SIG_%m-%d-%y_%H-%M-%S', time.localtime(secs))
                dirname = os.path.join(basedir, '{0}-{1:03d}'.format(ts, int(usecs/1000)))
                logging.info('New directory %s', dirname)
                try:
                    os.makedirs(dirname)
                except:
                    pass
                ofile = open(os.path.join(dirname, 'index.csv'), 'w')
                fields = ('seconds', 'microseconds', 'filename')
                index_file = csv.DictWriter(ofile,
                                            fieldnames=fields,
                                            quoting=csv.QUOTE_MINIMAL)
                index_file.writerow(dict(zip(fields, fields)))
            elif tag == 'metadata':
                metadata, timestamp, channels, scans = contents
                secs, usecs = [int(s) for s in timestamp.split('.')]
                filename = os.path.join(dirname, file_template.format(findex))
                logging.info('New datafile %s', filename)
                outf = open(filename, 'w')
            elif tag == 'data':
                idx, scan = contents
                outf.write(scan + '\n')
            elif tag == 'rawdata':
                idx, rawscan = contents
                outf.write(' '.join([str(e) for e in rawscan]) + '\n')
            elif tag == 'close':
                outf.close()
                index_file.writerow(dict(zip(fields, (secs, usecs, os.path.basename(filename)))))
                findex += 1
    except GeneratorExit:
        if ofile:
            ofile.close()
        if metadata:
            outf = open(os.path.join(dirname, 'metadata.json'), 'w')
            json.dump(metadata, outf, indent=4)
            outf.close()

def sign_extend(x):
    """
    Sign extend a vector to 24-bit values stored in 32-bit words
    """
    sign_bit = (1 << 23)
    mask = (1 << 24) - 1
    neg = (x & sign_bit) != 0
    pos = -neg

    tmp = x[neg] | ~mask
    x[neg] = tmp
    tmp = x[pos] & mask
    x[pos] = tmp
    return x
    
def parse_binary_file(filename, master_data, slave_data, block_size):
    """
    Generator to parse a raw binary data file and return the next
    scan on each iteration. Each scan is a numpy array of 32-bit
    integer values (A/D counts)
    """
    infile = open(filename, 'rb')
    m_chans, m_mask = master_data
    s_chans, s_mask = slave_data
    m_index = [((m_mask & (1 << i)) != 0) for i in range(m_chans)]
    s_index = [((s_mask & (1 << i)) != 0) for i in range(s_chans)]
    m_block_size = block_size*m_chans*4
    s_block_size = block_size*s_chans*4
    m_block = infile.read(m_block_size)
    while m_block:
        # Read a block of data, sign extend the values,
        # reformat as a 2-d matrix where each row is a 
        # separate scan, and remove the unused channels.
        s_block = infile.read(s_block_size)
        m_data = numpy.fromstring(m_block, dtype=numpy.int32)
        m_data = sign_extend(m_data).reshape((block_size, m_chans))
        m_data = numpy.compress(m_index, m_data, axis=1)
        if s_block:
            s_data = numpy.fromstring(s_block, dtype=numpy.int32)
            s_data = sign_extend(s_data).reshape((block_size, s_chans))
            m_data = numpy.hstack((m_data, numpy.compress(s_index, s_data, axis=1)))
        for row in m_data:
            yield row
            
def strip_ns(tag):
    """
    Remove XML namespace info from a tag
    """
    ns, tag = tag.split('}')
    return tag

def parse_metadata(elem):
    meta = {}
    for child in elem:
        try:
            value = eval(child.text)
        except NameError:
            value = child.text
        d = {'attrs': dict(child.attrib),
             'value': value}
        meta[strip_ns(child.tag)] = d
    return meta

def parse_attributes(elem):
    d = {}
    # All child elements must have an empty namespace
    for child in elem:
        try:
            d[child.tag] = eval(child.text)
        except NameError:
            d[child.tag] = child.text
    return d

def parse_file(infile, writer):
    n_chans = 0
    n_scans = 0
    bs = 0
    index = 0
    metadata = None
    master_data = None
    slave_data = None
    scan_source = None
    attributes = {}
    for event, element in etree.iterparse(infile,
                                          events=('start', 'end')):
        if (event, element.tag) == ('start', SEQ+'sequence'):
            writer.send(('sequence', element.get('timestamp')))
        elif (event, element.tag) == ('end', SEQ+'metadata'):
            metadata = parse_metadata(element)
        elif (event, element.tag) == ('end', SEQ+'attributes'):
            assert metadata is not None
            attributes = parse_attributes(element)
            metadata.update({'__attrs__': attributes})
        elif (event, element.tag) == ('end', SEQ+'channels'):
            n_chans = int(element.text)
            master_data = [int(e) for e in element.get('master').split(',')]
            slave_data = [int(e) for e in element.get('slave').split(',')]
        elif (event, element.tag) == ('end', SEQ+'scans'):
            n_scans = int(element.text)
        elif (event, element.tag) == ('end', SEQ+'block_size'):
            bs = int(element.text)
        elif (event, element.tag) == ('end', SEQ+'scan'):
            writer.send(('data', (index, element.text.strip())))
            index = index + 1
            element.clear()
        elif (event, element.tag) == ('start', SEQ+'trigger'):
            assert n_chans > 0 and n_scans > 0
            writer.send(('metadata', (metadata, element.get('timestamp'), n_chans, n_scans)))
            index = 0
            if scan_source is not None:
                for scan in scan_source:
                    writer.send(('rawdata', (index, scan)))
                    index = index + 1
                    if index == n_scans:
                        break
        elif (event, element.tag) == ('end', SEQ+'trigger'):
            writer.send(('close', None))
            element.clear()
        elif (event, element.tag) == ('end', SEQ+'datafile'):
            assert master_data is not None
            assert slave_data is not None
            assert bs > 0
            scan_source = parse_binary_file(element.text, master_data, slave_data, bs)

    writer.close()


def main():
    parser = OptionParser(usage=__doc__)
    parser.set_defaults(format='netcdf', basedir='.')
    parser.add_option('-f', '--format',
                      type='string',
                      dest='format',
                      help='output file format (default %default)')
    parser.add_option('-d', '--basedir',
                      type='string',
                      dest='basedir',
                      help='base directory (default %default)')

    opts, args = parser.parse_args()
    if len(args):
        infile = open(args[0], 'r')
    else:
        infile = sys.stdin

    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        datefmt='%Y/%m/%d %H:%M:%S')

    if opts.format == 'text':
        writer = text_file_writer(opts.basedir)
    else:
        writer = nc_file_writer(opts.basedir)
    parse_file(infile, writer)

if __name__ == '__main__':
    main()
