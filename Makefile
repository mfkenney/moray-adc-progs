#
# Makefile for MORAY ADC software.
#
CC = gcc
CFLAGS = -Wall -O2 -I. `pkg-config --cflags glib-2.0`

PROGS := testadc sams_recv moray_recv
testadc_SOURCES := testadc.vala

VALAC := valac
VAPI := moray_adc.vapi
VALA_CFLAGS = -v --thread --vapidir /usr/share/vala --vapidir .
PACKAGES = --pkg moray_adc --pkg posix --pkg stdio --pkg dbus-glib-1 --pkg gee-1.0

all: $(PROGS)

testadc: testadc.vala
	$(VALAC) $(VALA_CFLAGS) $(PACKAGES) $<

sams_recv: sams_recv.vala
	$(VALAC) $(VALA_CFLAGS) $(PACKAGES) $<

moray_recv: moray_recv.vala
	$(VALAC) $(VALA_CFLAGS) $(PACKAGES) $<

adc_trigger: adc_trigger.o
	$(CC) $(LDFLAGS) $^ -o $@ $(shell pkg-config --libs glib-2.0) -l24dsi_utils -l24dsi_dsl -lrt

clean:
	rm -f *.o *.so *~ *.la *.lo $(PROGS) testadc.c sams_recv.c
