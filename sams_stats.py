#!/usr/bin/env python
#
# Calculate statistics for each data-set (trigger)
#
"""
%prog [options] rms|mean|stdev samsfile

Calculate statistics for each data-set in a SAMS XML file.
"""
import sys
from lxml import etree
import numpy
from optparse import OptionParser


SEQ_NAMESPACE = "http://apl.uw.edu/oe/seq"
SEQ = '{%s}' % SEQ_NAMESPACE

def coroutine(func):
    def wrapper(*args, **kw):
        gen = func(*args, **kw)
        gen.next()
        return gen
    return wrapper

@coroutine
def calc_stats(n_chans, target):
    n = 0
    mean = numpy.zeros(n_chans, numpy.float)
    M2 = numpy.zeros(n_chans, numpy.float)
    try:
        while 1:
            scan = (yield)
            n = n + 1
            delta = scan - mean
            mean = mean + delta/n
            M2 = M2 + delta*(scan - mean)
    except GeneratorExit:
        variance = M2/n
        rms = numpy.sqrt(mean*mean + variance)
        d = {'rms': numpy.sqrt(mean*mean + variance),
             'stdev': numpy.sqrt(variance),
             'mean': mean}
        target.send((d))

@coroutine
def show_results(stat):
    while 1:
        result = (yield)
        output = ','.join(['%.6f' % e for e in result[stat]])
        print output

def strip_ns(tag):
    """
    Remove XML namespace info from a tag
    """
    ns, tag = tag.split('}')
    return tag

def parse_metadata(elem):
    meta = {}
    for child in elem:
        try:
            value = eval(child.text)
        except NameError:
            value = child.text
        d = {'attrs': dict(child.attrib),
             'value': value}
        meta[strip_ns(child.tag)] = d
    return meta

def parse_file(infile, stat='rms'):
    n_chans = 0
    scale = 0
    m = {}
    target = None
    for event, element in etree.iterparse(infile,
                                          events=('start', 'end')):
        if (event, element.tag) == ('start', SEQ+'trigger'):
            assert n_chans > 0
            assert scale > 0
            target = calc_stats(n_chans, show_results(stat))
        elif (event, element.tag) == ('end', SEQ+'metadata'):
            m = parse_metadata(element)
            scale = 2.0*m['vpeak']['value']/(1 << m['sample_size']['value'])
        elif (event, element.tag) == ('end', SEQ+'channels'):
            n_chans = int(element.text)
        elif (event, element.tag) == ('end', SEQ+'scan'):
            assert target is not None
            scan = numpy.array([float(e) for e in element.text.split()],
                               numpy.float)
            target.send((scan*scale))
            element.clear()
        elif (event, element.tag) == ('end', SEQ+'trigger'):
            if target:
                target.close()
            target = None
            element.clear()

def main():
    parser = OptionParser(usage=__doc__)

    opts, args = parser.parse_args()
    try:
        stat = args[0].lower()
        assert stat in ('rms', 'stdev', 'mean')
    except (IndexError, ValueError, AttributeError):
        parser.error('Missing argument')
    except AssertionError:
        parser.error('Invalid statistic selected')

    if len(args) > 1:
        infile = open(args[1], 'r')
    else:
        infile = sys.stdin

    parse_file(infile, stat)

if __name__ == '__main__':
    main()
